# Address for the IMAP server
imap_server = ""
# Username for the IMAP server
user_name = ""
# Password for the IMAP server
password = ""
# Required from address before making the markdown file
req_from = ""
# Path to store files in
post_path = ""
# Path to deployment script
deployment_script_path = ""
