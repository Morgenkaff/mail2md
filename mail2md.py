#!/usr/bin/python

# ext_vars holds credentials used to access the mail server
# and other bits and bops one might want to change for the specific implementation of mail2MD
from ext_vars import *
# Used to handle the mail server
import imaplib
# Used to handle "reading" the emails
import email
from email.header import decode_header
# Used to reformat the date from the email
import datetime
from datetime import datetime
# Used to test internet connection
import socket
# The os will be used for logging
import os
# And for deployment after creating the md files, subprocess wil be used
import subprocess
# Nedded re for the cleaning of the string
import re

# VARS:

# Set up the IMAP connection
imap = imaplib.IMAP4_SSL(imap_server)
imap.login(user_name, password)
imap.select("INBOX")

# Only read mails (usefull for debugging when testing for unread mails..)
only_read_mails = False

# FUNCTIONS:

# Start with a log function. Your future self will thank you..
# This one is pretty muc a copy of a log function I wrote way back
# takes a string, adds it to a text file in the directory,
# the code running is running in, and adds a date.
# The fancy bit is that it detects the file size, and creates a new log
# if the file is above 1MB.
def log(msg):
    log_file = "error.log"
    # If the file exists and is under 1MB:
    if not(os.path.isfile(log_file)) or (os.stat(log_file).st_size < 1000000):
        # Opening the file in append mode ('a)
        with open(log_file, "a") as text_file:
            # Print string to the file
            print(datetime.strftime(datetime.now(),"%d-%m-%Y - %H:%M:%S")+": "+msg, file=text_file)
        # Close the file again
        text_file.close()
    # if the file is above 1MB:
    elif os.stat(log_file).st_size > 1000000:
        # Move/rename the file to a backup.
        # In this case with name appended "backup" and the date
        # for the backup
        os.rename(log_file, log_file+".backup"+(datetime.strftime(datetime.now(),"%d-%m-%Y")))
        # Then the functions calls itself, but this time it will result
        # in a write to the log
        log(msg)

# Function that connects to a imap server, fetches the unread mail from a given address
# and returns a 2D array with date of the mail, subject and body text in the second dimension.
# If there are multiple unread mails, will they be added in the first dimension of said array.
# s = server to connect to
# n = username to log on with
# p = password to use
# f = from address to filter mails by
def get_mail(s, n, p, f):
    # Set up the IMAP connection
    server = imaplib.IMAP4_SSL(s)
    # Logging in with credentuals given in ext_vars.py
    server.login(n, p)
    # For this, I'm just goingto use the inbox
    server.select("inbox", readonly=only_read_mails)
    # Search for unread/unseen mails from address given in ext_var.py
    obj, data = server.search(None, '(FROM %s UNSEEN)' % f)
    # Create an list with length of the data[0]
    # I'm not completely shure why, but data[0].split() gives the amount af unread mails
    amount_of_unread= (len(data[0].split()))
    # With that, the list can be made
    return_mail = [None]*amount_of_unread
    # Loop through the results from search above
    # But before that, we hvae to create an index registration, that starts on 0 and increases 1 pr traversion
    index = 0
    for num in data[0].split():
        # Fetch mail in RFC822 format. I am not quite sure what that is.
        # But it works with the email library
        # I could have used BODY[0] to just get the body in plain text.
        # But I want to use the multiple parts of the mail, and it seemed easier
        # to just fetch the whole mail and hanife the different parts locally
        typ, data = server.fetch(num, '(RFC822)')
        # The email is stored as 'mail' in byte format
        mail = email.message_from_bytes(data[0][1])
        body = "";

        # Not all the test I have made with RFC822 seems to be multipart.
        # So lets check for that
        if mail.get_content_type() == "multipart/alternative":
            # Then for each part
            for part in mail.walk():
                # It checks for content type
                if (part.get_content_type() == 'text/plain') and (part.get('Content-Disposition') is None):
                    # when it finds a content type that is text,
                    body = part.get_payload(decode=True).decode('utf-8')
        elif mail.get_content_type() == "text/plain":
            # get the email body
            body = mail.get_payload(decode=True).decode('utf-8')

        # Getting different fields to use in post:
        # Getting the subject
        subject = (decode_header(mail['subject'])[0][0]).decode('utf-8')
        # Get the date from mail
        date = decode_header(mail["Date"])[0][0]

        # Now the mail can be stored as a list, in another list
        return_mail[index] = [date, subject, body]

        # last thing in the loop, should be to increment index with 1
        index += 1

    # Close the connection
    server.close()
    server.logout()

    # And return the 2D list with all the mails.
    return return_mail

# MARKDOWN PART:
# Function that creates text files formatted as posts for a Hugo web site,
# based on a 2D list given as parameter.
# the list should be formatted as:
# list[i]: [date, subject, body]
# It then writes the text files in the directory given in ext_vars.py
def create_file(m):

    # The first dimension of the list (the "mails") gets traversed
    for i in m:
        # The values for date, subject(/title for post) and body/text
        # gets extrapolated from second dimension in list
        date = i[0]
        subject = i[1]
        body = i[2]
        # Reformat dates from those given from imaplib to those used by
        # hugo eg "Sat, 20 May 2023 18:42:56 +0200" to "2023-05-20T18:42:56+02:00"
        # First making a datetime object from the string from imaplib
        date = datetime.strptime(date, "%a, %d %b  %Y %H:%M:%S %z")
        # Then a string from the datetime object.
        # Seems a bit elaborate, but I found it was
        # the easiest way to reformat a date string
        date_formatted = datetime.strftime(date,"%Y-%m-%dT%H:%M:%S%z")
        # For the specific theme/shortcodes I have on my site, it is
        # nescessary to add some text/shortcodes around the body
        # Taking the opportunity to strip the string here too
        content = "{{<poem>}}"+body.strip()+"{{< / poem>}}\n"
        # Then the complete string to make the markdown file with, is made
        string_to_print = (
"""
---
title: \"""" + subject + """\"
date: """ + date_formatted + """
hideSummary: true
author: \"Emil Harder\"
---

"""
+ content
        )
        # This print is usefull for debugging.. But should be commented
        # out in production
        print(string_to_print)

        # Wirting the string to a MD file:
        # Opening the file in write mode ('w')
        # Making a different string for the date, because windows gets
        # confused by colons
        file_date = datetime.strftime(date,"%d-%m-%Y %H-%M")
        # I might, from time to time, use naughty characters (æøå) in the subject,
        # and those should not be in a url. So lets clean the subject string:
        subject = re.sub('[^A-Za-z0-9]+', '', subject)
        print(subject)
        # print("Post path: "+post_path)
        with open(post_path+"/"+file_date+subject+"/_index.md", "w") as text_file:
            # Print string to the file
            print(string_to_print, file=text_file)
        # Close the file again
        text_file.close()

# This part is almost 100% copied from:
# https://stackoverflow.com/a/20913928
# Thank you miraculixx
def is_connected(hostname):
# It makes use of a exception handling:
# Try something - do something if it works - do something else if it does not
  try:
    # See if we can resolve the host name - tells us if there is
    # a DNS listening
    host = socket.gethostbyname(hostname)
    # Connect to the host - tells us if the host is actually reachable
    s = socket.create_connection((host, 993), 2)
    # Allways close the connection
    s.close()
    # If all that worked (no errors returneed) return True
    return True
# If any command in the "try-part" fails, the code "skips" to the
# exception part
  except Exception:
     pass # We ignore any errors, returning False
# This return statement is only reached if there have been errors in
# the connection. So it should return False
  return False

# Main function. Only runs if called from "if __name__ == "__main__":"
def main():

    # As this script only works (and makes sense) when connection to the internet,
    # it makes sense to test the conenction first
    # server is the server to test against should be an url
    # because the function is testing both dns lookup and connection
    server = imap_server
    # only run the get mail and create file functions if there is connection
    if is_connected(server):
        # Store the outout from get_mail in "mail" variable
        mail = get_mail(imap_server, user_name, password, req_from)
        # print(len(mail))
        # If there are mails (lenght of mail list is above 0)
        if (len(mail) > 0):
            # Print content of mail variable
            create_file(mail)
            # print("created file")
            # Lastly run a deployment script
            # It might be smarter to implement one in python. Do not know.
            # But I have one I use already. So I will just use that.
            # print(deployment_script_path)
            subprocess.call(['/usr/bin/sh', deployment_script_path])
    # If there was no connection, print an error log
    else:
        log("Could not connect to: "+server)

# If this script is runned as stand alone script, run main
if __name__ == "__main__":
    main()

# The end
