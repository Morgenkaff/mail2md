# Mail2MD
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


## Intend:

The goal is to fetch the newest mail in the Inbox (not in any subfolder) and save it as a markdown formatted file.

Maybe some filtering eg only save files from specific from address or such.
